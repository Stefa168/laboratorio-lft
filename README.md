[![licensebuttons by-nc-sa](https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/4.0)

# Leggi qui prima di proseguire
Ciao, mi chiamo Stefano.\
Devi essere finito qui perchè hai ricevuto il link da qualcuno, forse direttamente da me. 
Probabilmente starai cercando informazioni sul Progetto di Linguaggi Formali e Traduttori.

Questo repository contiene il mio progetto che ho svolto nel 2019.

Prima di tutto un avviso: il codice è sotto licenza [Creative Commons Attribuzione - Non commerciale - Condividi allo stesso modo 4.0 Internazionale (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.it). \
Questo significa che puoi perfettamente scorrere il codice, ma non puoi venderlo o appropriartene. Ci ho lavorato su parecchio e non mi dispiacerebbe che il lavoro venisse riconosciuto. Se fai modifiche, devi condividerle sotto questa licenza.\
Dai un'occhiata al link della licenza per maggiori informazioni. Quello che ho scritto qui sopra non è quello che per forza è scritto nella licenza e vale solo a scopo riassuntivo.

Il repository è diviso in 3 branch. Il master non è stato popolato, perchè ho preferito dividere gli esercizi dal progetto vero e proprio. Gli altri due branch dovrebbero essere autoesplicativi.

Voglio avvisare che in genere tendo a complicare le cose più del dovuto, quindi potrebbero esservi parti del codice che non coincidono con quelle della traccia del professore.

Lascio in master il PDF del progetto e degli esercizi. Buon lavoro!
